### CRUD & Standard Controller Actions
___
* CRUD를 웹개발에 맞게 세분화하여 만든 사이트
1. New    : 생성한 form을 사용자에게 보여주고 그 data를 전달한다.
2. Create : 전달 받은 data를 생성하는 것
3. Edit   : 수정한 form을 사용자에게 보여주고 그 data를 전달한다.
4. Update : 전달 받은 data로 수정한다.
5. Index : data들의 목록을 조회한다.
6. Show(Read) : 선택된 하나의 data를 상세하게 보여준다.
7. Delete(Destroy) : data를 삭제한다.