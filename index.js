// express 모듈을 가져오기
let express = require('express');
// server 로 express 객체를 생성
let server = express();

// JSON 형식의 데이터를 받아 사용할 수 있게 파싱한다.
server.use(express.json());
// POST 방식의 요청에 대해 body 데이터를 파싱하여 req.body 객체를 생성한다.
// urlencoded data 를 extended: false 이면 기본 내장된 queryString 모듈사용
// extended: true 이면 설치가 필요한 qs 모듈을 사용하여 쿼리스트링 파싱한다.
server.use(express.urlencoded({extended: false}));
// 정적파일들의 위치를 설정
// use 함수는 서버에서 요청이 올 때마다 무조건 콜백함수 실행됨
server.use(express.static(__dirname + '/static'));
// ejs 를 사용하기 위해 express view engine 에 djs 를 set 한다.
// ejs 는 동적파일들을 말하며 /views 디렉토리에 위치한다.
server.set('view engine', 'ejs');

/* HOME 을 외부 module 로 구분하여 불러온다. */
// ./routes/ 의 외부모듈로 가져온다.
server.use('/', require('./routes/home'));

/* Routers 을 외부 module 로 구분하여 불러온다. */
// ./routes/ 의 외부모듈로 가져온다.
server.use('/contacts', require('./routes/contacts'));


// server 를 외부와 연결할 수 있돌록 port 설정
server.listen(3000, () => {
    console.log('서버의 3000번 포트가 준비되었습니다!');
});