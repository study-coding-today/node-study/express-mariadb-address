let express = require('express');
let router = express.Router();

// Home
// '/' get 요청이 있으면 /contacts 로 보낸다.
router.get('/', (req, res) => {
    res.redirect('/contacts');
});

// 외부 module 로 사용하기 위해 router object 를 할당
module.exports = router;