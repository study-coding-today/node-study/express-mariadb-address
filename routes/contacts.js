let express = require('express');
let router = express.Router();
let mysql2 = require('mysql2');
let dbPool = mysql2.createPool({
    host: 'localhost',
    user: 'jgh',
    password: '123456',
    database: 'board',
    connectionLimit: 30,
    waitForConnections: true
});

/* Contacts */
// Index
// /contacts 로 get 요청이 오면 DB address table 을 다가져와 contacts/index 로
// contacts 객체를 반환한다.
router.get('/', (req, res) => {
    dbPool.query('SELECT * FROM address', (err, contacts) => {
        if (err) {
            return res.json(err);
        }
        res.render('contacts/index', {contacts: contacts});
    });
});

/* Contacts New */
// /contacts/new 로 get 요청이 오면 새로운 주소록을 만드는 from 이 있는
// views/contacts/new.ejs 를 render 한다.
router.get('/new', (req, res) => {
    res.render('contacts/new');
});

/* Contacts create */
router.post('/', (req, res) => {
    dbPool.query('INSERT INTO address (name, email, phone) VALUES (?, ?, ?)',
        [req.body.name, req.body.email, req.body.phone]);
    res.redirect('/contacts');
});

/* Contacts show */
router.get('/:id', (req, res) => {
    console.log(req.params.id);
    dbPool.query('SELECT * FROM address WHERE no=?', [req.params.id], (err, contact) => {
        if (err) {
            return res.json(err);
        }
        // DB 에서 가져오는 data 는 배열이다.
        console.log('show', contact[0]);
        res.render('contacts/show', {contact: contact[0]});
    });
});

/* Contacts edit */
router.get('/:id/edit', (req, res) => {
    dbPool.query('SELECT * FROM address WHERE no=?', [req.params.id], (err, contact) => {
        if (err) {
            return res.json(err);
        }
        console.log(contact)
        res.render('contacts/edit', {contact: contact[0]});
    });
});

/* Contacts update */
router.post('/:id', (req, res) => {
    dbPool.query('UPDATE address SET name=?, email=?, phone=? WHERE  no=?',
        [req.body.name, req.body.email, req.body.phone, req.params.id], (err, contact) => {
            if (err) {
                return res.json(err);
            }
            res.redirect('/contacts/' + req.params.id);
        });
});

/* Contacts Delete */
router.post('/:id/del', (req, res) => {
    dbPool.query('DELETE FROM address WHERE no='+ req.params.id, err => {
        if (err) {
            return res.json(err);
        }
        res.redirect('/contacts');
    });
});

module.exports = router;